<?php

require_once("../../../vendor/autoload.php");
use OOP\Penguin;
use OOP\Dove;
use OOP\Duck;
use OOP\Bird;
use OOP\canFly;
use OOP\canSwim;

$obj = new \OOP\Test();
$obj ->sayHello();
$obj ->sayWorld();
$obj ->sayExclamationMark();

echo "<hr>"; //end of Trait


$objAbstract = new \OOP\inherritedFromAbstractClass();
$objAbstract-> setMyValue("Hello Abstract class");
echo $objAbstract-> getMyValue();

echo "<hr>";//end of Abstract Class

function describeBird($bird){
    if($bird instanceof Bird){
        $bird->info();
    }
    if ($bird instanceof canFly){
        $bird->fly();
    }
    if($bird instanceof canSwim){
        $bird->swim();
    }

}
describeBird(new Penguin());
describeBird(new Duck());
describeBird(new Dove());

echo "<hr>";//end of Interface


//generator function start
function xyz(){
    for($i=1;$i<=5;$i++){
        yield $i;
    }
}
//generator function end

foreach (xyz() as $item) {
    echo $item;
}
