<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 2:43 PM
 */

namespace OOP;


abstract class AbstractClass
{
    // Force Extending class to define this method
    abstract public function getMyValue();
    abstract public function setMyValue($prefix);

    // Common method
    public function printOut() {
        print $this->getMyValue(). "\n";
    }


}


