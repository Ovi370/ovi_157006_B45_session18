<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 3:33 PM
 */

namespace OOP;


class Dove extends Bird implements canFly{
    public $name= "Dove";
    public function fly()
    {
        echo "I can fly <br>";
    }
}