<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 3:34 PM
 */

namespace OOP;


class Duck extends Bird implements canFly,canSwim{
    public $name = "Duck";
    public function fly()
    {
        echo "I can fly <br>";
    }
    public function swim()
    {
        echo "I can swim <br>";
    }
}