<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 2:03 PM
 */

namespace OOP;




trait Hello {
    public function sayHello(){
        echo "Hello";
    }

}
trait World {
    public function sayWorld(){
        echo "World";
    }
}

class Test{
    use Hello, World;
    public function sayExclamationMark(){
        echo "!";
    }
}




